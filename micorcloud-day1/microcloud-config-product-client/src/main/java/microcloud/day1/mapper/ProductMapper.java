package microcloud.day1.mapper;


import microcloud.day1.vo.Product;

import java.util.List;

public interface ProductMapper {
    boolean create(Product product);

    public Product findById(Long id);

    public List<Product> findAll();
}