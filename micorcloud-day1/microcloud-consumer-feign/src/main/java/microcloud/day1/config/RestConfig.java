//package microcloud.day1.config;
//
//import com.netflix.loadbalancer.IRule;
//import org.springframework.cloud.client.loadbalancer.LoadBalanced;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpHeaders;
//import org.springframework.web.client.RestTemplate;
//
//import java.nio.charset.Charset;
//import java.util.Base64;
//
//@Configuration
//public class RestConfig {
//    @Bean
////    开启负载均衡
//    @LoadBalanced
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
//
//    /**
//     * 拼接http头，使其具有权限
//     * @return
//     */
//    @Bean
//    public HttpHeaders getHeaders() { // 要进行一个Http头信息配置
//        HttpHeaders headers = new HttpHeaders(); // 定义一个HTTP的头信息
//        String auth = "admin:admin"; // 认证的原始信息
//        byte[] encodedAuth = Base64.getEncoder()
//                .encode(auth.getBytes(Charset.forName("US-ASCII"))); // 进行一个加密的处理
//        String authHeader = "Basic " + new String(encodedAuth);
//        headers.set("Authorization", authHeader);
//        System.out.println(headers.toString());
//        return headers;
//    }
//
//    /**
//     * 改变负载均衡的策略  全局
//     * @return
//     */
//    @Bean
//    public IRule ribbonRule() { // 其中IRule就是所有规则的标准
//        return new com.netflix.loadbalancer.RandomRule(); // 随机的访问策略
//    }
//
//
//}
