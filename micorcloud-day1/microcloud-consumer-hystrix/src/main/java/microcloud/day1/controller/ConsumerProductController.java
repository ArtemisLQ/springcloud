package microcloud.day1.controller;

import microcloud.day1.service.IProductClientService;
import microcloud.day1.service.IZUUlClientService;
import microcloud.day1.vo.Product;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/consumer")
public class ConsumerProductController {
//    public static final String PRODUCT_GET_URL = "http://MICROCLOUD-PROVIDER-PRODUCT/prodcut/get/";
//    public static final String PRODUCT_LIST_URL = "http://MICROCLOUD-PROVIDER-PRODUCT/prodcut/list/";
//    public static final String PRODUCT_ADD_URL = "http://MICROCLOUD-PROVIDER-PRODUCT/prodcut/add/";
//    @Resource
//    private RestTemplate restTemplate;
//
//    @Resource
//    private HttpHeaders httpHeaders;
//
//    /**
//     * 注入负载均衡客户端
//     */
//    @Resource
//    private LoadBalancerClient loadBalancerClient;
//
//
//    @RequestMapping("/product/get")
//    public Object getProduct(long id) {
//        Product product = restTemplate.exchange(PRODUCT_GET_URL + id, HttpMethod.GET, new HttpEntity<Object>(httpHeaders), Product.class).getBody();
//        return product;
//    }
//
//    @RequestMapping("/product/list")
//    public Object listProduct() {
//        //通过负载均衡客户端获取服务的相应信息
//        ServiceInstance serviceInstance = this.loadBalancerClient.choose("MICROCLOUD-PROVIDER-PRODUCT");
//        System.out.println(
//                "【*** ServiceInstance ***】host = " + serviceInstance.getHost()
//                        + "、port = " + serviceInstance.getPort()
//                        + "、serviceId = " + serviceInstance.getServiceId());
//
//        List<Product> list = restTemplate.exchange(PRODUCT_LIST_URL, HttpMethod.GET, new HttpEntity<Object>(httpHeaders), List.class).getBody();
//        return list;
//    }
//
//    @RequestMapping("/product/add")
//    public Object addPorduct(Product product) {
//        Boolean result = restTemplate.exchange(PRODUCT_ADD_URL, HttpMethod.POST, new HttpEntity<Object>(product, httpHeaders), Boolean.class).getBody();
//        return result;
//    }

    @Resource
    private IProductClientService iProductClientService;

    @Resource
    private IZUUlClientService izuUlClientService;

    @RequestMapping("/product/get")
    public Object getProduct(long id) {
        return iProductClientService.getProduct(id);
    }

    @RequestMapping("/product/list")
    public Object listProduct() {
        return iProductClientService.listProduct();
    }

    @RequestMapping("/product/add")
    public Object addPorduct(Product product) {
        return iProductClientService.addPorduct(product);
    }

    @RequestMapping("/product/getProductAndUser")
    public Object getProductAndUser(long id) {
        Map<String, Object> result = new HashMap();
        result.put("product", izuUlClientService.getProduct(id));
        result.put("user", izuUlClientService.getUsers(id + ""));
        return result;
    }


}
