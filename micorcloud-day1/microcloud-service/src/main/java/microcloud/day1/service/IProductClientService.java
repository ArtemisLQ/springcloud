package microcloud.day1.service;

import microcloud.day1.feign.FeignClientConfig;
import microcloud.day1.service.fallback.IProductClientServiceFallbackFactory;
import microcloud.day1.vo.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "MICROCLOUD-PROVIDER-PRODUCT", configuration = FeignClientConfig.class, fallbackFactory = IProductClientServiceFallbackFactory.class)
public interface IProductClientService {
    @RequestMapping("/prodcut/get/{id}")
    public Product getProduct(@PathVariable("id") long id);

    @RequestMapping("/prodcut/list")
    public List<Product> listProduct();

    @RequestMapping("/prodcut/add")
    public boolean addPorduct(Product product);

}
